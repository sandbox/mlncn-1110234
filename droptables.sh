#!/bin/bash
MDB="$1"

# Detect paths
MYSQL=$(which mysql)
AWK=$(which awk)
GREP=$(which grep)
  
if [ $# -ne 1 ]
then
 	echo "Usage: $0 {MySQL-Database-Name}"
 	echo "Drops all tables from a MySQL database."
 	exit 1
fi
  
echo "Are you absolutely certain you want to permanently delete all data in $MDB? (Y/n)"

read YN
if ( test "$YN" != "Y" )
then
  echo "Nothing deleted.";
  exit;
fi

TABLES=$($MYSQL $MDB -e 'show tables' | $AWK '{ print $1}' | $GREP -v '^Tables' )

C=0;
for t in $TABLES
do
 	echo "Deleting $t...";
 	$MYSQL $MDB -e "drop table $t";
  C=$((C+1));
done
echo "Deleted all $C tables from $MDB database."
