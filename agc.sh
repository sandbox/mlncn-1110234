#!/bin/bash
# Agaric Git Clone

# Any second parameter ("TRUE" or "new") will cause the script to initiate a 
# new project, rather than try to checkout an existing project.

# For the cd (change directory) to still apply for your convenience after the
# script is run, precede calling the script with a . (dot space).

if [ $# -lt 1 ]
then
  echo "USAGE: agc.sh projectname [new]";
  echo "  Agaric Git Clone requires at least one argument (the name of a project to fetch or create).  Optionally include a second argument to create a new project."
  return
fi

cd ~/code
if [ -n "$2" ]
then
  mkdir $1
  cd $1
  drush -y dl --drupal-project-rename=web
  mkdir web/sites/all/modules/contrib
  mkdir web/sites/default/files
  mkdir private_files
  sudo chown -R :www-data web/sites/default
  sudo chmod -R g+w web/sites/default
  sudo chown -R :www-data private_files
  sudo chmod -R g+w private_files
  cp ~/vlad/Rakefile .
  cp ~/vlad/.gitignore .
  git init
  git add .
  git commit -m "Drupal base and unmodified Rakefile."
else # Clone an existing site.
  git clone -o agaric git.agariclabs.org:/srv/git/$1.git
  cd $1
fi

cp web/sites/default/default.settings.php web/sites/default/settings.php

# If sudo required the above would have to be /home/ben not ~
ln -s ~/code/$1/web ~/workspace/$1

#create a new database with the project name.
~/scripts/newdb.sh $1
