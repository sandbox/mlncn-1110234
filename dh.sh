#!/bin/bash
# Drupal Head fetch

if [ $# -lt 1 ]
then
  echo "USAGE: dh.sh projectname [DEL]";
  echo "  Drupal Head fetch requires at least one argument (the name of the directory to put the latest version of Drupal HEAD in)."
  exit
fi

if [ $# -gt 1 ]
then
  if [ $2 == 'DEL' ]
    then
    echo "Are you SURE you want to permanently delete the directory and the database named $1?  If so type 'yes' in capital letters."
    read DOUBLECHECK
    if [ $DOUBLECHECK == 'YES' ]
      then
      mv ~/workspace/$1 /tmp/$1
      echo "Drupal head checkout $1 deleted (moved to /tmp)."
      ~/scripts/deldb.sh $1
    fi
    exit
  fi
fi

cd ~/workspace
cvs -z6 -d:pserver:anonymous:anonymous@cvs.drupal.org:/cvs/drupal checkout -d $1 drupal
cp $1/sites/default/default.settings.php $1/sites/default/settings.php
chmod 777 $1/sites/default/settings.php
mkdir $1/sites/default/files
mkdir $1/sites/default/private
mkdir $1/sites/default/private/files
chmod -R 777 $1/sites/default/files
chmod -R 777 $1/sites/default/private
# ln -s ~/code/$1 /var/www/$1

echo "Drupal HEAD has been downloaded to ~/workspace/$1."

# @TODO a better 
~/scripts/newdb.sh $1
