#!/bin/bash

proj=$1
date=$2

while read line
do
  cd ~/code/modules/$proj
  git cvsexportcommit -v -w ~/code/cvswc/$proj $line
  cd ~/code/cvswc/$proj
  cvs commit -F .msg
done < <(git log --after="$date" --reverse --pretty=format:%H;echo '')
