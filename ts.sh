#!/bin/bash
# Test site creator

# Any second parameter ("TRUE" or "dev") will cause the script to get the
# latest Drupal HEAD rather than the latest stable.

# For the cd (change directory) to still apply for your convenience after the
# script is run, precede calling the script with a . (dot space).

if [ $# -lt 1 ]
then
  echo "USAGE: $0 name [dev]";
  echo "  The test site creator script requires at least one argument (the name to give the Drupal site it creates).  Optionally include a second argument to create a new project."
  return
fi

user=admin
pass=admin
profile=standard

if [ -n "$2" ]
then
#@TODO 7.x-dev, or get it with git? 
  drush -y dl drupal-7.x-dev --drupal-project-rename=$1
else
  #download Drupal... can't believe drush gets drupal if given no argumens!
  drush -y dl --drupal-project-rename=$1
fi

#create a new database with the project name.
~/scripts/newdb.sh $1
#@TODO - if Drush is given the right su for the db, it can create the db
#@TODO actually turn this whole thing into a drush script that just calls si.

echo "The superuser account will have username '$user' and password '$pass'."
cd $1
drush -y site-install $profile --account-name=$user --account-pass=$pass --db-url=mysql://$1:$1@localhost/$1 --site-name=$1 test site
mkdir sites/all/modules/contrib
#mkdir sites/default/files
#mkdir private_files
#sudo chown -R :www-data web/sites/default
#sudo chmod -R g+w web/sites/default
#sudo chown -R :www-data private_files
#sudo chmod -R g+w private_files

#cp sites/default/default.settings.php sites/default/settings.php




